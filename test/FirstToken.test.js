const { BN, constants, expectEvent, expectRevert } = require('openzeppelin-test-helpers');

const { expect } = require('chai');

var FT = artifacts.require("./FirstToken.sol");
var tokenAddress;

contract('FT', accounts => {
    const decimals = new BN('2');
    const initialSupply = new BN('12000').mul(new BN('10').pow(decimals));

    const initialHolder = accounts[0]
    const recipient = accounts[1]
    const anotherAccount = accounts[2]

    let ftoken = null

	beforeEach(async function() {
		ftoken = await FT.new()
	})

    describe("approving transfers", function() {
        const to = recipient

        contract("transferring tokens to another user", function() {
            const amount = initialSupply;

            it('transfers FT to another account.', async function() {
                await ftoken.transfer(to, amount)

                expect(await ftoken.balanceOf(initialHolder)).to.be.bignumber.equal('0')

                expect(await ftoken.balanceOf(to)).to.be.bignumber.equal(amount)
            })
        })

        contract("allowing another user to spend on initial holder's behalf", function() {
            const approvedAmount = new BN(1000)

            it("approves spending from another account", async function() {
                await ftoken.approve(anotherAccount, approvedAmount)

                const allowance = await ftoken.allowance(initialHolder, anotherAccount)
                expect(allowance).to.be.bignumber.equal(approvedAmount)
            })

            it("increases the spend allowance for another account", async function() {
                const increaseBy = new BN(500)
                await ftoken.approve(anotherAccount, approvedAmount)

                await ftoken.decreaseAllowance(anotherAccount, increaseBy)

                const newAllowance = await ftoken.allowance(initialHolder, anotherAccount)
                expect(newAllowance).to.be.bignumber.equal(approvedAmount.sub(increaseBy))
            })

            it("decreases the spend allowance for another account", async function() {
                const decreaseBy = new BN(500)
                await ftoken.approve(anotherAccount, approvedAmount)

                await ftoken.decreaseAllowance(anotherAccount, decreaseBy)

                const newAllowance = await ftoken.allowance(initialHolder, anotherAccount)
                expect(newAllowance).to.be.bignumber.equal(approvedAmount.sub(decreaseBy))
            })
        })
    })
})
