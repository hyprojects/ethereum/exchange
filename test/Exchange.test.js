const { BN, constants, expectEvent, expectRevert } = require('openzeppelin-test-helpers');

const { expect } = require('chai');

var FT = artifacts.require("./FirstToken.sol");
var Exchange = artifacts.require("./Exchange.sol");

contract('Exchange', accounts => {
    const initialHolder = accounts[0]
    const recipient = accounts[1]
    const anotherAccount = accounts[2]

    let token = null;
    let exchange = null;

    beforeEach(async function() {
        token = await FT.new()

        const tokenAddress = token.address
        const tokenSymbol = await token.symbol()
        const tokenName = await token.name()

        exchange = await Exchange.new()
        exchange.registerToken(tokenAddress, tokenSymbol, tokenName)
	})

    describe("Registering tokens", function() {
        it("registers a token", async function() {
            const registeredToken = await exchange.registeredTokens(token.address)
            expect(registeredToken.symbol).to.be.equal("FT")
        })
    })

    describe("Moving Funds", function() {
        const to = recipient

        contract("Deposits", function() {
            it("transfers FT into the contract", async function() {
                const approvedAmount = new BN(1000)

                await token.approve(exchange.address, approvedAmount)
                const allowance = await token.allowance(initialHolder, exchange.address)

                await exchange.deposit(token.address, approvedAmount)

                const exchangeBalance = await token.balanceOf(exchange.address)

                expect(exchangeBalance).to.be.bignumber.equal(approvedAmount)
            })
        })
    })
})
