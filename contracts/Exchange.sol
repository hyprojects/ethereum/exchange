pragma solidity >=0.4.21 <0.6.0;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";

contract Exchange {
    using SafeMath for uint256;

    struct Token {
        string symbol;
        string name;
        bool tradable;
    }

    mapping (address => Token) public registeredTokens;

    mapping (address => mapping (address => uint256)) public balances;

    event Deposited(address token, address user, uint256 amount, uint256 balance);

    event Withdrawn(address token, address user, uint256 amount, uint256 balance);

    function registerToken(
        address _address,
        string memory _symbol,
        string memory _name) public {
        registeredTokens[_address] = Token(_symbol, _name, true);
    }

    function deregisterToken(address _address) public {
        Token memory registeredToken = registeredTokens[_address];
        registeredToken.tradable = false;

        registeredTokens[_address] = registeredToken;
    }

    /*
     * Call Token.approve before attempting to deposit. The contract will need to
     * have permission to transfer tokens on msg.sender's behalf.
     */
    function deposit(address _token, uint256 _amount) public {
        require(registeredTokens[_token].tradable);

        IERC20(_token).transferFrom(msg.sender, address(this), _amount);
        uint balance = balances[_token][msg.sender];

        balance = balance.add(_amount);

        balances[_token][msg.sender] = balance;

        emit Deposited(_token, msg.sender, _amount, balance);
    }

    function withdraw(address _token, uint256 _amount) public {
        uint balance = balances[_token][msg.sender];

        require(balance > 0 && balance >= _amount);

        // user can always withdraw from deregistered token.
        IERC20(_token).transferFrom(address(this), msg.sender, _amount);

        balance = balance.sub(_amount);

        balances[_token][msg.sender] = balance;

        emit Withdrawn(_token, msg.sender, _amount, balance);
    }
}
