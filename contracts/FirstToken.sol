pragma solidity >=0.4.21 <0.6.0;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";

contract FirstToken is ERC20, ERC20Detailed {
    constructor()
        ERC20Detailed("FirstToken", "FT", 2)
        public {
        uint256 initialBalance = 12000 * (10 ** uint256(decimals()));
        _mint(msg.sender, initialBalance);
    }
}
