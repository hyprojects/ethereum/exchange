const FirstToken = artifacts.require("FirstToken");
const Exchange = artifacts.require("Exchange");

module.exports = function(deployer) {
  deployer.deploy(FirstToken);
  deployer.deploy(Exchange);
};
